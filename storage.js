class Storage {
    static getSearchedUsersFromStorage(){
        //Tüm kullanıcı al
        let users;
        
        if (localStorage.getItem("searched")=== null) {
            users=[];
        }
        else{
            users=JSON.parse(localStorage.getItem("searched"));

        }
        return users;
    }
    static addSeachedUserToStogare(username){
        // Kullanıcı ekle
        let users=this.getSearchedUsersFromStorage();
        //İndex of
        if(users.indexOf(username)===-1){
            users.push(username);
        }
        localStorage.setItem("searched",JSON.stringify(users));
    }
    static clearAllSearchedUsersFromStogare(){
        //Tüm  kullanıcılari sil
        localStorage.removeItem("searched");

        }
}
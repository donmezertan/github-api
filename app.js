//Elementleri seçme
const githubForm=document.getElementById("github-form");
const nameInput=document.getElementById("github-name");
const clearLastUsers=document.getElementById("clear-last-users");

const lastUsers=document.getElementById("last-users");

const github=new Github();
const ui=new UI();
eventListeners();


function eventListeners() {
    githubForm.addEventListener("submit",getData);
    clearLastUsers.addEventListener("click",clearAllSearched);
    document.addEventListener("DOMContentLoaded",getAllSearched);
}

function getData(e) {
    let username=nameInput.value.trim();
    if (username === " "){
        alert("Lütfen geçerli bir kullanıcı adı giriniz.");
    }
    else{
        github.getGithubData(username)
            .then(response =>{
                
                if (response.user.message ==="Not Found"){
                    //Hata mesajı
                    console.log("Hatalı Kullanıcı adı Girdiniz");
                    ui.showError("Kullanıcı Bulunamadı");
                }
                else{
                    ui.addSearchedUserToUI(username);
                    Storage.addSeachedUserToStogare(username);
                    ui.showUserInfo(response.user);
                    ui.showRepoInfo(response.repo);
                }
            })
            .catch(err =>ui.showError(err) );
    }
    ui.clearInput(); //Input Temizleme
    e.preventDefault();
}
function clearAllSearched() {
    // Tüm arananlar Temizle
    if(confirm("Emin misiniz?")){
        //Silme işlemleri
        Storage.clearAllSearchedUsersFromStogare(); //Storagedan temizliycek
        ui.clearAllSearchedFromUI();
    }
}

function getAllSearched() {
    //Arananları storage al ve Ui ekle

    let users=Storage.getSearchedUsersFromStorage();

    let result=" ";
    users.forEach(user=>{
        result+=`<li class="list-group-item">${user}</li>`;
    });
    lastUsers.innerHTML=result;
}